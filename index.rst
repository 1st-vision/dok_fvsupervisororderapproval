.. |label| replace:: Vorgesetzten-Regelung für Bestellungen
.. |snippet| replace:: FvSupervisorOrderApproval
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.3.0
.. |maxVersion| replace:: 5.3.3
.. |version| replace:: 1.1.0
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Voraussetzungen:
Das Modul "FvUserPermissions" muss installiert und aktiviert sein.

OfficeLine:
Beim Ansprechpartner wird ein USER-Feld mit der Email des Vorgesetzten angelegt. Die Email wird in der Shop-Tabelle "fv_user_s_user" überspielt.

Shop:
Wenn ein Mitarbeiter die Berechtigung "Vorgesetztenregelung" aktiviert hat und die Email des Vorgesetzten in der Tabelle "fv_user_s_user" vorhanden ist, dann müssen seine Bestellungen vom Vorgesetzten genehmigt werden.
Der Vorgesetzte erhält eine Email, dass eine neue Bestellung „zum freigeben“ vorliegt. Durch Hyperlink in der Email kann die Bestellung freigegeben oder storniert werden. Diese Email enthält auch die Bestelldetails.
Reminder an den Vorgesetzten nach 48 Stunden (im Intervall) mit allen Bestellungen im Status "Genehmigen".
Je Bestellung wird eine separate Mail verschickt.

Frontend
--------
Modul-Funktionalität:
-	Wenn ein User die „Vorgesetzten-Regelung“ aktiviert hat (Berechtigung + Email vom Vorgesetzten), dann greift dieses Prozedere:

Hinweis dass seine Bestellung erst geprüft werden muss (am Ende vom „Checkout“). Nach dieser Bestellung bekommt er die automatische Bestellmail vom Shop nicht. Er kriegt sie erst später.

.. image::  fvsupervisororderapproval8.jpg

-	An den Vorgesetzten wird diese Mail geschickt. Im Anhang befindet sich die HTML-Ansicht der Bestellmail (wird einfach im Browser automatisch geöffnet)

.. image::  fvsupervisororderapproval9.jpg

-	Wenn er auf einen der 2 Links klickt, dann wird die Bestellung storniert oder frei gegeben (je nach dem wo er hin klickt). Er kommt auf diese Seite und sieht wieder die Mail der Bestellung. Für diese Funktionalität ist kein Login erforderlich.

.. image::  fvsupervisororderapproval0.jpg

-	Danach wird der User (der Besteller) per Email informiert. Als Anhang ist wieder die Bestellmail.

.. image::  fvsupervisororderapproval1.jpg

Backend
-------
Hier können Sie das Plugin konfigurieren.

-	Hier müssen Sie einstellen wie der „Key“ der Berechtigung für die „Vorgesetzen-Regelung“ heißt. Standardmäßig steht der Wert „fv_supervisor_order_approval“ im Feld. D.h. Sie müssen einfach eine neue Berechtigung in der Datenbank anlegen (wie Bild unten) und diese dem jeweiligen User zuweisen + natürlich die E-Mail vom Vorgesetzten in der Tabelle ‚ fv_user_s_user ‘ eintragen.

.. image::  fvsupervisororderapproval5.jpg

-	Diese Berechtigung müssen Sie manuell einstellen und den Key in den Einstellungen eintragen 

.. image::  fvsupervisororderapproval6.jpg

-	Sie können die Berechtigung auf „administrierbar“ stellen, dann erscheint sie auch hier.

.. image::  fvsupervisororderapproval7.jpg


technische Beschreibung
------------------------

Folgendes passiert bei der Modul-Installation:
Das Modul setzt voraus dass „User-Berechtigungen für Konzerne“ installiert und aktiviert ist ansonsten lässt es sich nicht installieren.

-	Neuer Status „zur Genehmigung“ wird angelegt; dieser Status wird verwendet für die Bestellungen, die geprüft werden müssen.

.. image::  fvsupervisororderapproval1.jpg

-	Die Tabelle „fv_user_s_user“ wird um die Spalte „fv_supervisor_order_approval_email“ erweitert; diese enthält die E-Mail vom Vorgesetzen. Achtung das Modul prüft die E-Mail auf Gültigkeit! Wenn „bla bla“ hier eingetragen ist, dann greift die "Vorgesetzten-Regelung“ nicht.

.. image::  fvsupervisororderapproval2.jpg

-	2 Mail-Templates werden angelegt (Email an den Vorgesetzten bei der Bestellung und Email an den Besteller nach dem der Vorgesetzte auf einen der Links geklickt hat)

.. image::  fvsupervisororderapproval3.jpg

-	Cronjob (wird täglich ausgeführt): Reminder an den Vorgesetzten nach 48 Stunden

.. image::  fvsupervisororderapproval4.jpg

Bei Bestätigung wird eine Mail an den Shopbetreiber abschickt.

Modifizierte Template-Dateien
-----------------------------
:/checkout/finish.tpl:




